
# Loja

---

# Manual

## The instanciation : `t = Loja(path [, flags [, mode]])`

Open a database, with the following parameters:

- the string `path` is the directory in which the database files reside.
  This directory must already exist and be writable.
- the optional integer `flags` is the special options for this environment.
  This parameter must be set to 0 or by bitwise OR'ing together one or more options
  (see details on the documentation of `mdb_env_open`)
- the optional integer `mode` is the UNIX permissions to set on created files and semaphores.
  This parameter is ignored on Windows.

## The data access

The indexation as a Lua table is transparent (thanks to metamethods `__index`, `__newindex` & `__pairs`).

Keys and values are serialized before storage.
`Loja.codec` is the name of the module used for encoding/decoding them,
by default ['ubjson'](https://ubjson.org/)
is used. This module must supply two functions : `encode` & `decode` like typical JSON module.

## The call to methods of the underlying LMDB `env` : `t('meth' [, ...])`

For details, see the documentation of [lightningmdb](https://github.com/shmul/lightningmdb)
& [LMDB](http://symas.com/lmdb).

## Global constants & function

Loja inherits from lightningmdb. So, all constants `MBD_*` and the function `version` are available.

# Examples

```lua
local Loja = require'Loja'

os.execute('mkdir -p my/path/to/database')
local t = Loja('my/path/to/database', 0, tonumber('0666', 8))

-- data access
t.foo = 'Foo'
t.bar = 42
t.baz = 3.14
for k, v in pairs(t) do
    print(k, v)
end
print(t.foo)

-- call to underlying methods
print(t('get_path'))
local info = t('info')
print(info.me_mapsize)
local stat = t('stat')
print(stat.ms_entries)

-- globals
print(Loja.codec)      -- ubjson
print(Loja.version())  -- version LMDB
print(Loja.MDB_RDONLY) -- constant
```

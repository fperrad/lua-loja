
# lua-Loja

---

## Overview

This module is built on the top of [lightningmdb](https://github.com/shmul/lightningmdb),
a Lua wrapper around [LMDB](http://symas.com/lmdb).

With this module, a [LMDB](http://symas.com/lmdb)
(Lightning Memory-Mapped Database) ie. a key/value store on disk, is mapped on a Lua table.

Keys and values are serialized/stringified before storage (by default, with the [UBJSON](https://ubjson.org/)
format), so they could be more complex than primitive type.

## Status

lua-Loja is in alpha stage.

It's developed for Lua 5.2, 5.3 & 5.4 (rely on the metamethod *__pairs*).

## Download

lua-Loja source can be downloaded from
[Framagit](https://framagit.org/fperrad/lua-loja).

## Installation

lua-Loja depends on [lightningmdb](https://github.com/shmul/lightningmdb).

lua-Loja is available via LuaRocks:

```sh
luarocks install lua-loja
```

or manually, with:

```sh
make install
```

## Test

The test suite requires the module
[lua-TestAssertion](https://fperrad.frama.io/lua-testassertion/).

    make test

## Copyright and License

Copyright &copy; 2024 Fran&ccedil;ois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

#!/usr/bin/env lua

require 'Test.Assertion'

-- setup
os.execute('rm -rf ./tmp/01-basic')
os.execute('mkdir -p ./tmp/01-basic')

plan(8)

local Loja = require 'Loja'

local t = Loja('./tmp/01-basic')

same( t, {}, "new and empty" )

is_nil( t.foo )

t.foo = 'Foo'
t.bar = 42
t.baz = 3.14

same( t, {
    foo = 'Foo',
    bar = 42,
    baz = 3.14,
}, "first insertions" )

t.foo = nil
is_nil( t.foo, "remove one" )

same( t, {
    bar = 42,
    baz = 3.14,
} )

t.bar = true
t.baz = false

same( t, {
    bar = true,
    baz = false,
} )

t.foo = {
   1,
   2,
   3,
   4,
}

same( t, {
    foo = { 1, 2, 3, 4 },
    bar = true,
    baz = false,
} )

t.foo = {
    'Foo',
    bar = { 'Bar' },
    baz = { 'Baz' },
}

same( t, {
    foo = {
--        'Foo',  squashed by ubjson
        bar = { 'Bar' },
        baz = { 'Baz' },
    },
    bar = true,
    baz = false,
} )

-- cleanup
os.execute('rm -rf ./tmp/01-basic')


#!/usr/bin/env lua

require 'Test.Assertion'

if _VERSION < 'Lua 5.4' then
    skip_all("only 5.4+")
end

-- setup
os.execute('rm -rf ./tmp/03-toclose')
os.execute('mkdir -p ./tmp/03-toclose')

plan(1)

load([[
local Loja = require 'Loja'

do
    local t <close> = Loja('./tmp/03-toclose')

    same( t, {}, "new and empty" )
end
]])()

-- cleanup
os.execute('rm -rf ./tmp/03-toclose')


#!/usr/bin/env lua

require 'Test.Assertion'

-- setup
os.execute('rm -rf ./tmp/02-pairs')
os.execute('mkdir -p ./tmp/02-pairs')

plan(1)

local Loja = require 'Loja'

local db = Loja('./tmp/02-pairs')

local t1 = {
    foo = 'Foo',
    bar = 42,
    baz = 3.14,
    array = { 1, 2, 3, 4 },
    hash = {
        abc = true,
        def = false,
    },
}

for k, v in pairs(t1) do
    db[k] = v
end

local t2 = {}
for k, v in pairs(db) do
    t2[k] = v
end

same( t1, t2 )

-- cleanup
os.execute('rm -rf ./tmp/02-pairs')


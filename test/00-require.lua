#!/usr/bin/env lua

require 'Test.Assertion'

plan(15)

if not require_ok 'Loja' then
    BAIL_OUT "no lib"
end

-- setup
os.execute('rm -rf ./tmp/00-require')
os.execute('mkdir -p ./tmp/00-require')

local m = require 'Loja'
is_table( m )
equals( m, package.loaded.Loja )

equals( m._NAME, 'Loja', "_NAME" )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'persistence', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )

equals( m.codec, 'ubjson', 'codec' )

matches( m.version(), '^LMDB %d%.%d%.%d+:', 'version LMDB' )
is_number( m.MDB_RDONLY, 'import MBD_*' )

local o = m('./tmp/00-require', 0, tonumber('0666', 8))
is_table( o, 'instance' )

equals( o('get_path'), './tmp/00-require', 'env:get_path()' )
is_table( o('info'), 'env:info()' )
is_table( o('stat'), 'env:stat()' )

-- cleanup
os.execute('rm -rf ./tmp/00-require')

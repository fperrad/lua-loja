
lua-Loja : transparent persistence with a key/value store
=========================================================

Introduction
------------

With this module, a [LMDB](http://symas.com/lmdb)
(Lightning Memory-Mapped Database) ie. a key/value store on disk, is mapped on a Lua table.

Keys and values are serialized/stringified before storage (by default, with the [UBJSON](https://ubjson.org/)
format).

Links
-----

The homepage is at <https://fperrad.frama.io/lua-loja>,
and the sources are hosted at <https://framagit.org/fperrad/lua-loja>.

Copyright and License
---------------------

Copyright (c) 2024 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

